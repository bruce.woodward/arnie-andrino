void setup() {

        // to indicate a pin is output 
        //YOU CAN ASSIGN WHICH PINS YOU WANT TO USE
        pinMode(6, OUTPUT); 
        pinMode(7, OUTPUT);
        pinMode(8, OUTPUT); 
        pinMode(9, OUTPUT);
        pinMode(13, OUTPUT);


        Serial.begin(9600);  // Start bluetooth serial at 9600, MAKE sure your   bluetooth is set to 9600 baud
        // or change this value to match your bluetooth baud rate     
}

int state = HIGH;

void flop()
{
        if (state == HIGH)
                state = LOW;
        else
                state = HIGH;
}

void loop() 
{


        char data = Serial.read(); 

        if (data == '1') {
                digitalWrite(6, state);
                delay(1000);
                flop();
                digitalWrite(6, state);
                delay(1000);
        }

        /* save this code for later

        if (data == '1') digitalWrite(6, HIGH);
        if (data == '0') digitalWrite(6, LOW);

        if (data == '3') digitalWrite(7, HIGH);
        else digitalWrite(7, LOW);
        delay(1000); // Wait for 1000 millisecond(s)
        digitalWrite(13, LOW);
        delay(1000); // Wait for 1000 millisecond(s)



        if (data == '2') digitalWrite(7, LOW);


        if (data == '5') digitalWrite(8, HIGH);
        if (data == '4') digitalWrite(8, LOW); 

        if (data == '7') digitalWrite(9, HIGH);
        if (data == '6') digitalWrite(9, LOW);

        if (data == '9') digitalWrite(13, HIGH);
        if (data == '8') digitalWrite(13, LOW);

        */

}


